<!--
 * @Author: tangwei
 * @Date: 2021-02-22 22:57:20
-->
# 介绍

> `ent-web`是[ent]的前端项目工程，本项目基于Vue + Vue-Router + Vuex + Axios + Element-UI开发。

# 使用

- 安装依赖
```shell
npm install
```
- 运行
```shell
npm run dev
```
- 打包
```shell
npm run build
```
- 切换node版本
```shell
nvm use 8.11.4
```

# 配置修改
- 请求地址
    将`static\js\config.js`中`baseUrl`值改成自己的baseUrl即可。
# ent-web


