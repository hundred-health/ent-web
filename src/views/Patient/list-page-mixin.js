import HmTable from '@/views/Core/HmTable'
import HmButton from '@/views/Core/HmButton'
import TableColumnFilterDialog from '@/views/Core/TableColumnFilterDialog'
import { mapState, mapGetters } from 'vuex'
import { format } from '@/utils/datetime'
export default {
  inheritAttrs: false,
  components: {
    HmTable,
    HmButton,
    TableColumnFilterDialog
  },
  props: {},
  data () {
    return {
      tableHeight: null, // 表格高度，动态计算
      tableLoading: false, // 表格加载状态
      filters: {}, // 表格过滤条件，待覆盖
      columns: [],
      filterColumns: [],
      pageRequest: { pageNum: 1, pageSize: 20 },
      // 分页数据
      pageResult: {},
      pageApi: null, // 待覆盖
      deleteComfirmMsg: '是否确认删除该数据？'
    }
  },
  created () {
    this.fetchDics()
  },
  mounted () {
    this.findPage(null)
    let headerBarHeight = this.$global.headerHeight
    let tabHeight = this.$global.tabHeight
    let spaceHeight = this.$global.spaceHeight
    let searchHeight = this.$refs.searchDiv.offsetHeight
    let titleHeight = this.$refs.titleDiv.offsetHeight
    let pagingBarHeight = this.$global.pagingBarHeight
    // window.innerHeight:浏览器的可用高度
    this.tableHeight = window.innerHeight - headerBarHeight - tabHeight - spaceHeight - searchHeight - titleHeight - pagingBarHeight
    // 赋值vue的this
    const that = this
    // window.onresize中的this指向的是window，不是指向vue
    window.onresize = () => {
      return (() => {
        that.tableHeight = window.innerHeight - headerBarHeight - tabHeight - spaceHeight - searchHeight - titleHeight - pagingBarHeight
      })()
    }
    this.initColumns()
  },
  beforeMount () {
  },
  beforeDestroy () {
  },
  methods: {
    fetchDics () {
      /**
       * 请求字典数据
       */
      if (!this.getDictByType) {
        // 获取字典
        this.dicProps.user.forEach(prop => {
          this.$store.dispatch('dict/getDictByType', prop)
        })
      }
    },
    // 查询用户 重置页码为第一页
    findFirstPage: function () {
      console.log('=====>findFirstPage')
      // 重置页码为1
      this.pageRequest.pageNum = 1
      // 查询用户列表
      this.findPage(null)
    },
    // 获取分页数据
    findPage: function (data) {
      this.tableLoading = true
      if (data !== null) {
        this.pageRequest = data.pageRequest
      }
      // filters复制到pageRequest
      Object.assign(this.pageRequest, this.filters)
      console.log('=====>findPage.pageRequest', this.pageRequest)
      this.pageApi.findPage(this.pageRequest).then((res) => {
        console.log('=====>findPage.res', res)
        if (res.status === 1) {
          this.pageResult = res.data
        } else {
          this.$message({ message: '查询失败,' + res.msg, type: 'error' })
        }
        this.tableLoading = false
      })
    },
    // 表列格式化：时间格式化
    dateFormat: function (row, column, cellValue, index) {
      return format(row[column.property])
    },
    // 处理表格列过滤显示
    initColumns: function () {
      // this.columns = [
      //   // {prop: 'id', label: 'ID', minWidth: 50},
      //   {prop: 'realName', label: '用户姓名', minWidth: 120, required: true},
      //   {prop: 'telephone', label: '手机号码', minWidth: 100, required: true},
      //   {prop: 'roleNames', label: '角色', minWidth: 100, formatter: this.roleFormat},
      //   {prop: 'status', label: '状态', minWidth: 70, formatter: this.statusFormat}
      // ]
      // this.filterColumns = this.columns // JSON.parse(JSON.stringify(this.columns))
    },
    // 操作通用回调方法
    callBack: function (res, successMsg) {
      if (res.status > 0) {
        this.findPage(null)
        if (successMsg) {
          this.$message({ message: successMsg, type: 'success' })
        }
      } else {
        this.$message({message: res.msg, type: 'error'})
      }
    },
    // 删除
    handleDelete: function (data) {
      this.$confirm(this.deleteComfirmMsg, '提示', {
        type: 'warning'
      }).then(() => {
        this.pageApi.remove({ id: data.id }).then((res) => {
          this.callBack(res, '删除成功')
        })
      }).catch((err) => {
        console.error('=====> handleDelete.error', err)
      })
    },
    // 换页刷新
    refreshPageRequest: function (pageNum) {
      this.pageRequest.pageNum = pageNum
      this.findPage(null)
    },
    handleSizeChange (val) {
      this.pageRequest.pageSize = val
      this.findPage(null)
    }
  },
  computed: {
    ...mapState('dict', [
      'dicProps'
    ]),
    ...mapGetters('dict', {
      getDic: 'getDic'
    }),
    getDeptInfo () {
      return this.getDic('deptDict')
    },
    getTitleInfo () {
      return this.getDic('titleDict')
    }
  },
  watch: {
    tableHeight (val) {
      this.tableHeight = val
    }
  }
}
