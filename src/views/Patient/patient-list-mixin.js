import ListPageMixin from './list-page-mixin'
import EditDialogContent from './EditDialogContent'
import { format } from '@/utils/datetime'
export default {
  inheritAttrs: false,
  props: {},
  mixins: [ListPageMixin],
  components: { EditDialogContent },
  data () {
    return {
      filters: {
        patientName: '', // 患者姓名
        phone: '', // 联系方式
        cardNo: '' // 身份证号
      }, // 表格过滤条件，待覆盖
      pageApi: this.$api.patient,
      deleteComfirmMsg: '是否确认删除该患者资料？',
      importDlgConfig: {
        visible: false
      },
      importFormData: {
        fileList: []
      },
      editDlgConfig: {
        type: '', // new: 新增;edit: 编辑
        title: '', // 新增或编辑
        visible: false,
        initData: null // 初始化数据，打开dialog时赋值
      }
    }
  },
  created () {
  },
  mounted () {
  },
  beforeMount () {
  },
  beforeDestroy () {
  },
  methods: {
    handleDownTemplate () {
      // 创建a标签，用于跳转至下载链接
      const tempLink = document.createElement('a')
      tempLink.style.display = 'none'
      tempLink.href = window.baseUrl + '/excel/PatientInfo.xlsx'
      tempLink.setAttribute('download', decodeURI('患者模板'))
      // 兼容：某些浏览器不支持HTML5的download属性
      if (typeof tempLink.download === 'undefined') {
        tempLink.setAttribute('target', '_blank')
      }
      // 挂载a标签
      document.body.appendChild(tempLink)
      tempLink.click()
      document.body.removeChild(tempLink)
    },
    handleImport () {
      this.importDlgConfig.visible = true
    },
    submitUpload () {
      console.log('importDlgConfig.fileList', this.importDlgConfig.fileList)
      this.$refs.upload.submit()
    },
    // handleRemove (file, fileList) {
    //   console.log(file, fileList);
    // },
    // handlePreview (file) {
    //   console.log(file);
    // },
    handleChange (file, fileList) {
      this.importDlgConfig.fileList = fileList.slice(0)
      console.log('handleChange.fileList', file, fileList)
    },
    handleUpload (params) {
      console.log('handleUpload', params)
      this.pageApi.upload(params).then((ret) => {
        if (ret.status === -1) {
          this.$message.warning(ret.msg || '导入患者时发生错误，请检查模板内容')
        }
        if (ret.status === 1) {
          this.$message.success(ret.msg || '导入患者成功')
        }
      })
    },
    // 显示新增界面
    handleAdd () {
      this.editDlgConfig.type = 'new'
      this.editDlgConfig.title = '新增'
      this.editDlgConfig.visible = true
      this.editDlgConfig.initData = {
        patientCode: null, // 患者编号
        // 拼音缩写
        patientName: null, // 患者姓名
        testCode: null, // 受试者编号
        cardNo: null, // 身份证号
        birthday: null, // 出生日期
        gender: null, // 性别
        phone: null, // 联系方式
        height: null, // 身高
        weight: null, // 体重
        // 籍贯
        address: null, // 现住地址
        outpatientNo: null, // 门诊号
        inpatientNo: null, // 住院号
        signState: null, // 签署知情同意书
        signDate: format(new Date())
      }
    },
    handleSubmited () {
      this.editDlgConfig.visible = false
      this.findFirstPage()
    },
    // 显示编辑界面
    handleEdit (row) {
      this.editDlgConfig.type = 'edit'
      this.editDlgConfig.title = '编辑'
      this.editDlgConfig.visible = true
      this.editDlgConfig.initData = Object.assign({}, row)
    },
    handleDetail (row) {
      this.editDlgConfig.type = 'view'
      this.editDlgConfig.title = '查看'
      this.editDlgConfig.visible = true
      this.editDlgConfig.initData = Object.assign({}, row)
    },
    handlePatientCase (row) {
      this.$message('该功能尚未实现')
    },
    // 删除 override
    handleDelete: function (data) {
      this.$confirm(this.deleteComfirmMsg, '提示', {
        type: 'warning'
      }).then(() => {
        this.pageApi.remove({ patientCode: data.patientCode }).then((res) => {
          this.callBack(res, '删除成功')
        })
      }).catch((err) => {
        console.error('=====> handleDelete.error', err)
      })
    }
  },
  computed: {}
}
