/*
 * 接口统一集成模块
 * @Author: tangwei
 * @Date: 2021-02-22 22:05:14
 */

import * as login from './moudules/login'
import * as user from './moudules/user'
import * as menu from './moudules/menu'
import * as log from './moudules/log'
import * as role from './moudules/role'
import * as dict from './moudules/dict'
import * as form from './moudules/form'
import * as userRelation from './moudules/userRelation'
import * as patient from './moudules/patient'

// 默认全部导出
export default {
  login,
  user,
  menu,
  log,
  role,
  dict,
  form,
  userRelation,
  patient
}
