/*
 * 用户权限关联管理
 * @Author: tangwei
 * @Date: 2021-07-13 22:30:48
 */

import { get, post } from '../axios'

// 查询未选择用户
export const findUnselectedList = (data) => {
  return post('/system/user/relation/unselected', data)
}

// 查询已选择用户
export const findSelectedList = (data) => {
  return post('/system/user/relation/selected', data)
}

// 批量删除
export const batchDelete = (data) => {
  return post('/system/user/relation/remove', data)
}

// 批量添加
export const batchAdd = (data) => {
  return post('/system/user/relation/batchAdd', data)
}
