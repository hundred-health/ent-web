/**
 * 患者管理模块
 * @Date: 2021年7月14日16:31:35
 */
import { get, post } from '../axios'

// 分页查询
export const findPage = (data) => {
  return post('/bus-patient/page-query', data)
}

export const upload = (data) => {
  const forms = new FormData()
  const configs = {
    headers: {'Content-Type': 'multipart/form-data'}
  }
  forms.append('file', data.file)
  return post('/bus-patient/import-template', forms, configs)
}

// // 查找用户的菜单权限标识集合
// export const findPermissions = (params) => {
//   return get('/system/user/permissions/{username}', null, params)
// }
// // 根据用户名获取用户信息
// export const getUserInfo = (params) => {
//   return get('/system/user/userInfo/{username}', null, params)
// }
// // 锁定账户
// export const lock = (params) => {
//   return post('/system/user/lock/{id}', null, params)
// }
// // 恢复账户
// export const unlock = (params) => {
//   return post('/system/user/unlock/{id}', null, params)
// }
// // 初始化密码
// export const initPassword = (params) => {
//   return post('/system/user/initPassword/{id}', null, params)
// }
// 新增
export const save = (data) => {
  return post('/bus-patient/save', data)
}

// // 修改
// export const update = (data) => {
//   return post('/system/user/update', data)
// }
// export const updateById = (params, pathVariable) => {
//   return post('/system/user/update/{id}', params, pathVariable)
// }

// 删除
export const remove = (params) => {
  return post('/bus-patient/delete/{patientCode}', null, params)
}
