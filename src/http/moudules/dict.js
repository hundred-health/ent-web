/*
 * 字典管理模块
 * @Author: tangwei
 * @Date: 2021-03-08 21:30:55
 */
import { get, post } from '../axios'

// 通过字典分类获取字典项
export const findDictByType = (params) => {
  return get('/system/dict/items/{classCode}', null, params)
}

// 获取字典项目分类
export const findDictClasses = (params) => {
  return get('/system/dict/classes', null, params)
}