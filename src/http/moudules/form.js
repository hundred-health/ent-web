/*
 * 表单配置API
 *
 * @Author: tangwei
 * @Date: 2021-07-04 19:57:25
 */
import { get, post } from '../axios'

// 获取表单配置信息
export const getFormConfigList = (params) => {
  return get('/form/config/list/{tableName}', null, params)
}

// 隐藏
export const hide = (data) => {
  return post('/form/config/hide', data)
}

// 显示
export const show = (data) => {
  return post('/form/config/show', data)
}

// 添加扩展列
export const add = (data) => {
  return post('/form/config/addExt', data)
}

// 修改扩展列
export const update = (data) => {
  return post('/form/config/updateExt', data)
}

// 删除扩展列
export const remove = (data) => {
  return post('/form/config/deleteExt', data)
}