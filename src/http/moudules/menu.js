/*
 * @Description:
 * @Author: tangwei
 * @Date: 2021-03-14 14:12:16
 */
import { get, post } from '../axios'

/*
 * 菜单管理模块
 */
// 查找导航菜单树
export const findNavTree = (params) => {
  return get('/system/resource/navTree/{username}', null, params)
}
// 获取所有资源树
export const findResourceTree = () => {
  return post('/system/resource/list')
}
// 根据查询条件获取资源树
export const findTreeBySearchText = (data) => {
  return post('/system/resource/tree', data)
}
export const findResTreeByRoles = (data) => {
  return post('/system/resource/resTree/roles', data)
}