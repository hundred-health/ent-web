/*
 * @Author: tangwei
 * @Date: 2019-09-09 19:09:15
 */
import app from './app'
import menu from './menu'
import user from './user'
import iframe from './iframe'
import tab from './tab'
import dict from './dict'

export default {
  app,
  menu,
  user,
  iframe,
  tab,
  dict
}