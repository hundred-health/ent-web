/*
 * @Author: tangwei
 * @Date: 2021-02-22 22:05:14
 */
import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    dicProps: { // 字典项合集
      organ: [ // 机构
        { typeCode: '1001001', typeName: 'level' }, // 医疗机构级别
        { typeCode: '1001002', typeName: 'classification' } // 医疗机构类型
      ],
      user: [ // 用户信息
        { typeCode: '1002001', typeName: 'deptDict' }, // 用户科室
        { typeCode: '1002002', typeName: 'titleDict' } // 用户头衔
      ],
      form: [ // 表单配置
        { typeCode: '1006001', typeName: 'dataTable' } // 用户表单
      ]
    },
    dictByType: {} // 以type存储的字典数据
  },
  getters: {
    /**
     * 返回字典列表
     * @author tangwei add(20191017)
     */
    getDic: state => prop => {
      /**
       * 根据字典名称获取对应数据
       * @param {String} prop 字典对应的typeName
       */
      return state.dictByType[prop] || []
    }
  },
  mutations: {
    updateDic (state, data) {
      /**
       * 更新字典项目
       * @param {Object} data data.name 字典名称  data.data 字典数据
       */
      Vue.set(state.dictByType, data.name, data.data)
    }
  },
  actions: {
    /**
     * 通过字典类型获取字典项数据
     * @param {object} context({commit}使用了对象的解构)
     * @param {object} payload 请求参数
     */
    getDictByType ({commit}, payload) {
      let _this = Vue.prototype
      _this.$api.dict.findDictByType({
        classCode: payload.typeCode
      }).then((res) => {
        if (!res.data) return
        commit('updateDic', {
          name: payload.typeName,
          data: res.data
        })
      })
    }
  }
}