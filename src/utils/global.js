/*
 * 全局常量、方法封装模块
 * 通过原型挂载到Vue属性
 * 通过 this.$global 调用
 *
 * @Author: tangwei
 * @Date: 2021-02-22 22:05:14
 */

// 后台管理系统服务器地址
// export const baseUrl = 'http://localhost:8088/ent/'
export const baseUrl = window.baseUrl
// 系统数据备份还原服务器地址
export const backupBaseUrl = 'http://localhost:8002'
// 页面标题头高度（对应src\style\theme\_size.scss）
export const headerHeight = 60
// tab标签高度
export const tabHeight = 40
// 空白间隔高度
export const spaceHeight = 9
// 列表分页栏高度
export const pagingBarHeight = 32
/**
 * 判断是否为空
 * @param str 字符串
 */
const isEmpty = (str) => {
  if (!str || str === undefined) { return true } else { return false }
}
/**
 * 判断是否为整数
 * @param {Object} val
 */
const isPositiveInt = (val) => {
  var numReg = /^[0-9]*$/
  var numRe = new RegExp(numReg)
  if (numRe.test(val)) {
    return true
  } else {
    return false
  }
}

/**
 * @description 绑定事件 on(element, event, handler)
 */
const on = (function () {
  if (document.addEventListener) {
    return function (element, event, handler) {
      if (element && event && handler) {
        element.addEventListener(event, handler, false)
      }
    }
  } else {
    return function (element, event, handler) {
      if (element && event && handler) {
        element.attachEvent('on' + event, handler)
      }
    }
  }
})()

/**
 * @description 解绑事件 off(element, event, handler)
 */
export const off = (function () {
  if (document.removeEventListener) {
    return function (element, event, handler) {
      if (element && event) {
        element.removeEventListener(event, handler, false)
      }
    }
  } else {
    return function (element, event, handler) {
      if (element && event) {
        element.detachEvent('on' + event, handler)
      }
    }
  }
})()

export default {
  baseUrl,
  backupBaseUrl,
  isEmpty,
  isPositiveInt,
  headerHeight,
  tabHeight,
  spaceHeight,
  pagingBarHeight,
  on
}
