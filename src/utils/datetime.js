/** *
 * @Author: tangwei
 * @Date: 2020-09-15 09:12:08
 */
/** *
 * @Author: tangwei
 * @Date: 2020-09-15 09:12:08
 */
/**
 * 时间日期相关操作
 */
import * as moment from 'moment'
/**
 * 时间格式化
 * 将 2018-09-23T11:54:16.000+0000 格式化成 2018/09/23 11:54:16
 * @param datetime 国际化日期格式
 */
export function format (datetime) {
  return formatWithSeperator(datetime, '/', ':')
}

/**
 * 时间格式化
 * 将 2018-09-23T11:54:16.000+0000 格式化成类似 2018/09/23 11:54:16
 * 可以指定日期和时间分隔符
 * @param datetime 国际化日期格式
 */
export function formatWithSeperator (datetime, dateSeprator, timeSeprator) {
  if (datetime != null) {
    const dateMat = new Date(datetime)
    const year = dateMat.getFullYear()
    const month = dateMat.getMonth() + 1
    const day = dateMat.getDate()
    const hh = dateMat.getHours()
    const mm = dateMat.getMinutes()
    const ss = dateMat.getSeconds()
    const timeFormat = year + dateSeprator + month + dateSeprator + day + ' ' + hh + timeSeprator + mm + timeSeprator + ss
    return timeFormat
  }
}

/**
 * 时间格式化
 * 将 2018-09-23T11:54:16.000+0000 格式化成 2018-09-23
 * @param datetime 国际化日期格式
 */
export function formatYYYYMMDD (datetime) {
  if (datetime != null) {
    // const dateMat = new Date(datetime)
    // const year = dateMat.getFullYear()
    // const month = dateMat.getMonth() + 1
    // const day = dateMat.getDate()
    // const timeFormat = year + '-' + month + '-' + day
    return moment(datetime).format('YYYY-MM-DD')
  }
}

/**
 * 获取当前星期几
 * @param {日期} date
 */
export function getWeek (date) { // 参数时间戳
  let week = moment(date).day()
  switch (week) {
    case 1:
      return '星期一'
    case 2:
      return '星期二'
    case 3:
      return '星期三'
    case 4:
      return '星期四'
    case 5:
      return '星期五'
    case 6:
      return '星期六'
    case 0:
      return '星期日'
  }
}

/**
 * 获取本周一的日期
 */
export function getThisMonday () {
  // 计算今天是这周第几天
  var weekOfday = moment().format('E')
  // 周一日期
  return moment().subtract(weekOfday - 1, 'days')
}

/**
 * 日期加减（天数）
 */
export function addDay (date, num) {
  return moment(date).add(num, 'd')
}